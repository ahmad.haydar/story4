$( function() {
    $( "#accordion" ).accordion({
      collapsible: true,
      header: "> div > h3"
    });
    $(".up,.down").click(function(event){
        event.stopPropagation(); 
        const currentButton = $(this);
        const accordionBlock = currentButton.parent().parent();
        if(currentButton.is(".up")) accordionBlock.insertBefore(accordionBlock.prev())
        else accordionBlock.insertAfter(accordionBlock.next())
    });
    
    $('#playMovie1').click(function(){
  $('#playMovie1').get(0).play();
      });

  } );