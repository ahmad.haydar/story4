from django.shortcuts import render
def index(request):
    return render(request, 'story1/index.html')

def profile(request):
    context = {
        "type" : "story1"
    }
    return render(request, 'story1/profile.html',context)

def secret(request):
    return render(request, 'story1/secret.html')
# Create your views here.
