from django.urls import path
from . import views

app_name = 'story1'

urlpatterns = [
    path('', views.index, name='index'),
    path('profile/', views.profile, name='profile'),
    path('secret/', views.secret, name='secret')
    # dilanjutkan ...
]