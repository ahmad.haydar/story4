from django.shortcuts import render
def index(request):
    context = {
        "type" : "home"
    }
    return render(request, 'homepage/profile.html', context)
def gallery(request):
    context = {
        "type" : "gallery"
    }
    return render(request, 'homepage/gallery.html', context)
# Create your views here.
